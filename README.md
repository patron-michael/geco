# GECO

Evolutionary genomics analysis pipeline. Extract data on genome composition, codon usage, genomic features and compare
the genome of interest against closely related species and genera. 


## Getting started

Access to project files
```
git clone https://gitlab.com/patron-michael/geco.git
```

Put files in corresponding directory. Each species need to have:
- cds fasta file in cds_genomic directory
- gene Bank genome summary file in geneBank_summary directory
- protein fasta file in protein_fasta directory
- eggNog xlsx file from eggNog analyse in eggNog directory

## In working

D-genies analyse in not functional and need to be manually perform
for the moment.