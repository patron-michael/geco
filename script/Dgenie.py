# -*- coding: utf-8 -*-
# @Time : 11/28/2022 9:05 PM
# @Author : Juejun CHEN
# Modified by Michael PATRON

from Bio import SeqIO
from readpaf import parse_paf
import collections
import warnings
import dgenies

import pandas as pd

warnings.filterwarnings(action="ignore", category=UserWarning, module="pandas")


def run_dgenies():
    dgenies.launch(mode="webserver").run()


class FindGene:
    """
    This script is used  to generate structural information of motif result,
    It will return two excel tables,
    one is about the global domain result, another is about the details.
    """

    @staticmethod
    def import_gb(gb_file):
        gb_dict = collections.defaultdict(list)
        gb_df = pd.DataFrame(columns=['locus_tag', 'gene', 'product', 'note'])
        n = 0
        for rec in SeqIO.parse(gb_file, "genbank"):
            for feature in rec.features:
                if feature.type == "CDS":
                    line = ''
                    line += str(feature.location.start) + ','
                    line += str(feature.location.end) + ','
                    cds_id = str(feature.qualifiers['locus_tag'][0])

                    if 'gene' in feature.qualifiers.keys():
                        line += str(feature.qualifiers['gene']).replace('[', '').replace(']', ',').replace("'", '')
                    if 'product' in feature.qualifiers.keys():
                        line += str(feature.qualifiers['product']).replace('[', '').replace(']', ',').replace("'", '')
                    if 'note' in feature.qualifiers.keys():
                        line += str(feature.qualifiers['note']).replace('[', '').replace(']', ',').replace("'", '')

                    if 'protein_id' in feature.qualifiers.keys():
                        line += str(feature.qualifiers['protein_id']).replace('[', '').replace(']', ',').replace("'",
                                                                                                                 '')

                    lines = line.split(',')
                    gb_dict[cds_id] = lines
        gb_df.set_index('locus_tag', inplace=True)
        return gb_dict

    def import_dgenie_result(self, dgenie_file, cds_file, n):
        # transfers paf_file to pandas
        with open(dgenie_file, "r") as handle:
            df = parse_paf(handle, dataframe=True)

        # Récupérer les colonnes nécessaire et trier par la longueur de cassures
        self.paf_df = df[['query_name', 'query_length', 'query_start', 'query_end']]
        self.paf_df['cassure_length'] = self.paf_df['query_end'] - self.paf_df['query_start']
        self.paf_df.sort_values('cassure_length', inplace=True, ascending=False)
        self.paf_df = self.paf_df[0:n]
        self.paf_df.set_index('query_name', inplace=True)
        return self.paf_df

    def find_gene_in_zone(self, gb_dict, paf_df, name, output):
        result_dict = collections.defaultdict(list)
        n = 1
        for index, row in paf_df.iterrows():
            for key, value in gb_dict.items():
                if value[0] > str(row['query_start']) and value[1] < str(row['query_end']):
                    protein_information = f"{gb_dict[key][2]}"
                    result_dict[n].append(protein_information)
            n += 1

        result_df = pd.DataFrame.from_dict(result_dict, orient='index')


        gene_number_dict = dict()
        n = 1
        for k, v in result_dict.items():
            gene_number_dict[n] = len(v)
            n += 1

        result_df.insert(0, 'gene_sum', gene_number_dict.values())

        paf_df['gene_sum'] = gene_number_dict.values()

        self.export_df(paf_df, result_df, output)

    @staticmethod
    def export_df(df1, df2, filename):
        df1.to_excel(filename, index=True)
        with pd.ExcelWriter(filename) as writer:
            df1.to_excel(writer, sheet_name='paf_result')
            df2.to_excel(writer, sheet_name='paf_gene_detail_result')
