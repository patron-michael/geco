import os


def blastn_hit(queries: dict, species_data: dict):
    print("Blastn")
    target_path = queries.get("Target")
    total_length_target = 0
    for key, query in queries.items():
        # if key != "target":
        output = f"../result/Blastn_target_vs_other/{key}_blastn.csv"
        species_data[key][f"ANI_AF_{key}"] = output

        # os.system(f"sudo apt install ncbi-blast+")
        # os.system(f"touch {output}")

        os.system(
            f"blastn -query {target_path} -subject {query} -outfmt '6 qseqid sseqid qstart qend sstart send qcovhsp "
            f"pident evalue bitscore' -qcov_hsp_perc 80 -perc_identity 30 -out {output}")

        if key == "Target":
            with open(output, 'r') as file_read:
                for line in file_read.readlines():
                    line = line.split("\t")
                    length = int(line[3]) - int(line[2])
                    total_length_target += int(length)

        out_string = ""
        total_length = 0
        total_bbh_iden_align = 0

        with open(output, 'r') as file_read:
            for line in file_read.readlines():
                line = line.split("\t")
                length = int(line[3]) - int(line[2])
                identity = line[7]
                total_length += int(length)
                total_bbh_iden_align += (float(identity) / 100) * float(length)

        with open(output, 'r') as file_read:
            for line in file_read.readlines():
                line = line.split("\t")
                numerator = total_bbh_iden_align
                denominator = total_length

                ANI = numerator / denominator
                AF = numerator / total_length_target
                line[9] = line[9].replace("\n", "")

                line.append(str(ANI))
                line.append(str(AF) + "\n")
                for element in line:
                    if element != line[len(line) - 1]:
                        out_string += element + ","
                    else:
                        out_string += element

            out_string = 'QUERY ID,SUBJECT ID,START QUERY,END QUERY,START SUBJECT,END SUBJECT,QUERY COVERAGE,' \
                         'PERCENT IDENTITY,E VALUE,BIT SCORE,ANI,AF\n' + out_string

        with open(output, 'w') as file:
            file.writelines(out_string)


def blastp_hit(queries: dict, species_data: dict):
    print("Blastp")
    target_path = queries.get("Target")
    for key, query in queries.items():
        if key != "Target":
            output = f"../result/Blastp_target_vs_other/{key}_blastp.csv"
            species_data[key][f"AAI_{key}"] = output

            # os.system(f"sudo apt install ncbi-blast+")
            # os.system(f"touch {output}")

            os.system(
                f"blastp -query {target_path} -subject {query} -outfmt '6 qseqid sseqid qstart qend sstart send "
                f"qcovhsp pident evalue bitscore' -qcov_hsp_perc 80 -out {output}")

            total_length_target = 0
            if key == "Target":
                with open(output, 'r') as file_read:
                    for line in file_read.readlines():
                        line = line.split("\t")
                        length = int(line[3]) - int(line[2])
                        total_length_target += int(length)

            out_string = ""
            total_length = 0
            total_bbh_iden_align = 0
            with open(output, 'r') as file_read:
                for line in file_read.readlines():
                    line = line.split("\t")
                    length = int(line[3]) - int(line[2])
                    identity = line[7]
                    total_length += int(length)
                    total_bbh_iden_align += (float(identity) / 100) * float(length)

                with open(output, 'r') as file_read:
                    for line in file_read.readlines():
                        line = line.split("\t")
                        numerator = total_bbh_iden_align
                        denominator = total_length

                        AAI = numerator / denominator
                        line[9] = line[9].replace("\n", "")

                        line.append(str(AAI) + "\n")

                        for element in line:
                            if element != line[len(line) - 1]:
                                out_string += element + ","
                            else:
                                out_string += element

                    out_string = 'QUERY ID,SUBJECT ID,START QUERY,END QUERY,START SUBJECT,END SUBJECT,QUERY COVERAGE,' \
                                 'PERCENT IDENTITY,E VALUE,BIT SCORE,AAI,AF\n' + out_string

                with open(output, 'w') as file:
                    file.writelines(out_string)


def export_df(queries: dict, filename: str):
    metrics = {}
    for key, queries in queries.items():
        if str(key).startswith('ANI_AF'):
            head = str(key).replace('ANI_AF_', "")
            metrics[head] = ""

        with open(queries, 'r') as file:
            if str(key).startswith('ANI_AF'):
                name = str(key).replace('ANI_AF_', "")
                header = file.readline()
                line = file.readline()
                line = line.split(",")
                AF = line[11]
                ANI = line[10]
                metrics[name] = str(AF) + "," + str(ANI)
                metrics[name] = metrics[name].replace("\n", "")

            if str(key).startswith('AAI_'):
                name = str(key).replace('AAI_', "")
                header = file.readline()
                line = file.readline()
                line = line.split(",")
                AAI = line[10]
                metrics[name] = metrics[name] + "," + str(AAI)

    metrics.pop('Target')

    output_string = "Name,AF,ANI,AAI\n"
    content = ""
    for key, value in metrics.items():
        content += key + "," + value
    output_string = output_string + content

    with open(filename, 'w') as file:
        file.writelines(output_string)
