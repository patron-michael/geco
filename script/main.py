import os

import numpy as np
import pandas as pd

from script.Dgenie import FindGene
from script.Dgenie import run_dgenies

from arborescence_init import tree_initialisation
from script.GECO_research import *
from COG_NCBI import *
from Genomic_signature import blastn_hit, blastp_hit, export_df
from Emboss import Emboss
from Requirements import requirements_initialisation
import subprocess

if __name__ == "__main__":

    # region preparation
    requirements_initialisation("requirements.txt")

    # Emboss tools
    if subprocess.call(["compseq", "-version"]) != 0:
        os.system('sudo apt install emboss')
    if subprocess.call(["cusp", "-version"]) != 0:
        os.system('sudo apt install emboss')
    if subprocess.call(["cai", "-version"]) != 0:
        os.system('sudo apt install emboss')

    # NCBI tools
    if subprocess.call(["blastn", "-version"]) != 0:
        os.system('sudo apt install ncbi-blast+')
    if subprocess.call(["blastp", "-version"]) != 0:
        os.system('sudo apt install ncbi-blast+')

    species_data = tree_initialisation()
    # endregion

    # region Emboss compseq
    for key, element in species_data.items():
        Emboss.compseq_run(species_data[key]["genomic_fasta"], species_data[key]["comp_seq"], 2)
    # endregion

    # region Emboss Codon usage
    data_ranges = {}
    for key, element in species_data.items():
        Emboss.codon_usage_run(
            species_data[key]["cds_genomic"],
            species_data[key]["Codon_usage_cusp"],
            species_data[key]["Codon_usage_cai"])

        data_ranges[key] = Emboss.cai_range(species_data[key]["Codon_usage_cai"])
        print(key, "cai score mean", Emboss.cai_mean(species_data[key]["Codon_usage_cai"]))

        Emboss.cai_search_corresponding_protein(species_data[key]["Codon_usage_cai"], species_data[key]["cds_genomic"],
                                                key)

    cai_ranges_dataframe = pd.DataFrame.from_dict(data_ranges)
    Emboss.cai_range_plot(cai_ranges_dataframe, save=True, path="../result/CAI_comparing.png", show=False)
    # endregion

    # # region EggNog
    # # paths_gene_bank = []
    # # for key, value in species_data.items():
    # #     paths_gene_bank.append(species_data[str(key)]["geneBank_summary"])
    # #
    # # # TODO faire eggNog
    # #
    # # paths_eggNog = []
    # # for key, value in species_data.items():
    # #     paths_eggNog.append(species_data[str(key)]["eggNog"])
    # #
    # # geco = CreateTable(paths_gene_bank, paths_eggNog)
    # # result_list = geco.apply_to_genBank_list()
    # # geco_df = geco.create_df(result_list)
    # # egg_list = geco.apply_to_eggNog_list()
    # # egg_df = geco.create_egg_df(egg_list)
    # #
    # # # Build NCBI COG result of interest genome
    # # COG_file = species_data["Target"]["cog_database"]
    # #
    # # ncbi_cog = NCBI_COG()
    # # class_df = ncbi_cog.count_COG_class(COG_file)
    # # result_name = '../result/COG_classe_list.xlsx'
    # # ncbi_cog.export_df(class_df, result_name)
    # #
    # # geco.export_df(df1=geco_df, df2=egg_df, df3=class_df, filename="../result/GECO_result.xlsx")
    # # endregion
    #
    # # TODO: voir si possible de faire D-genie en cli (https://pypi.org/project/dgenies/) // --> peut lancer le serveur en webserveur ou standalone, pas de cli disponible
    #
    # # # TODO Pour BBH  il faudrait faire une comparaison dans les deux sens
    # # # TODO pour core genome comparaison AllvsAll
    # # # TODO comparer les hit des csv entre eux pour extraire le core genome
    #
    # # region D-Genies
    # for key, value in species_data.items():
    #     if key != "Target":
    #         paf_espece_interet = species_data[str(key)]["paf_file"]
    #         database_gene = species_data[str(key)]["geneBank_summary"]
    #         cds_file = species_data[str(key)]["cds_genomic"]
    #
    #         Dgenie_result = FindGene()
    #         paf_df = Dgenie_result.import_dgenie_result(paf_espece_interet, cds_file, 10)
    #         gb_dict = Dgenie_result.import_gb(database_gene)
    #
    #         Dgenie_result.find_gene_in_zone(gb_dict, paf_df, key, f"../result/d-genies/breakings/{key}.xlsx")
    # # endregion
    #
    # # region Blast & alignment metric
    # queries_blastn = {}
    # queries_blastp = {}
    # for key, value in species_data.items():
    #     queries_blastn[str(key)] = species_data[str(key)]["cds_genomic"]
    #
    # for key, value in species_data.items():
    #     queries_blastp[str(key)] = species_data[str(key)]["protein_fasta"]
    #
    # blastn_hit(queries_blastn, species_data)
    #
    # blastp_hit(queries_blastp, species_data)
    #
    # queries_metrics = {}
    # for key, value in species_data.items():
    #     for key2, element in value.items():
    #         if key2.startswith("ANI_AF_"):
    #             queries_metrics[key2] = element
    #         if key2.startswith("AAI_"):
    #             queries_metrics[key2] = element
    #
    # output = "../result/Genomic-metrics.csv"
    # export_df(queries_metrics, output)
    # endregion