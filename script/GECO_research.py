# -*- coding: utf-8 -*-
# @Time : 2022/10/7 0:21
# @Author : Juejun CHEN
import string

import pandas as pd
from Bio import SeqIO


class CreateTable:
    """
    class use to extract interested information of interested specie from Genbank file
    """

    def __init__(self, file_genBank_list, file_eggCog_list):
        """ initialize the file list """
        print("Species Metrics")
        self.file_genBank_list = file_genBank_list
        self.file_eggCog_list = file_eggCog_list
        self.result_list = []
        self.result_list_cog = []
        self.specie = []

    def import_COG(self, cog_file):
        """
        TODO: importer le tableau de COG,
              récupérer le gene ID et stocker dans un tableau,
              relier avec la fonction de count_nb() pour compter nombre protéine COG
        :param cog_file:
        :return:
        """
        self.COG_data = pd.read_csv(cog_file, header=None, on_bad_lines='skip')
        self.COG_geneID = self.COG_data[[1]]
        self.COG_geneID.set_index(self.COG_data[6], inplace=True)

        return self.COG_data

    def count_nb(self, file):
        summary_dict = {}
        cds, rRna, tRna = 0, 0, 0
        CDS_length_list = []
        nb_annotation = 0
        nb_transduction = 0
        gene = 0
        nb_pseudo = 0
        nb_replication, nb_recombination, nb_repair = 0, 0, 0
        nb_methyl, nb_transport_aa = 0, 0
        seq_c = 0
        seq_g = 0

        for rec in SeqIO.parse(file, "genbank"):

            seq_g = rec.seq.count("G")
            seq_c = rec.seq.count("C")


            self.specie.append(rec.description)
            summary_dict["Specie"] = rec.description
            summary_dict["length"] = len(rec.seq)
            for k,v in rec.annotations['structured_comment'].items():
                try:
                    nb_annotation = int(v.get("Genes (coding)").replace(',', ''))
                    cds = int(v.get('CDSs (total)').replace(',', ''))
                except:
                    pass

            if rec.features:
                for feature in rec.features:
                    # Coding density = nbr de genes total / nb de base
                    if feature.type == "gene":
                        gene += 1
                    if feature.type == "CDS":

                        if cds == 0:
                            cds += 1
                        length = feature.location.end - feature.location.start
                        CDS_length_list.append(length)
                        for tag in feature.qualifiers:
                            if tag == "pseudo":
                                nb_pseudo += 1
                            if tag == "protein_id" and nb_annotation == 0:
                                # if ("incomplete" or "partial") not in str(feature.qualifiers[tag]):
                                    nb_annotation += 1
                            if "transduction" in str(feature.qualifiers):
                                nb_transduction += 1
                            if "replication" in str(feature.qualifiers):
                                nb_replication += 1
                            if "recombination" in str(feature.qualifiers):
                                nb_recombination += 1
                            if "repair" in str(feature.qualifiers):
                                nb_repair += 1
                            if "methylation" in str(feature.qualifiers):
                                nb_methyl += 1

                    if feature.type == "rRNA":
                        rRna += 1
                    if feature.type == "tRNA":
                        tRna += 1

        summary_dict["pseudogene"] = nb_pseudo
        summary_dict["gene"] = gene
        summary_dict["gene coding density%"] = round((sum(CDS_length_list) / summary_dict["length"])*100, 2)
        summary_dict["CDS"] = cds
        summary_dict["max CDS"] = max(CDS_length_list)
        summary_dict["min CDS"] = min(CDS_length_list)
        summary_dict["moyenne CDS"] = round(sum(CDS_length_list) / len(CDS_length_list), 2)
        summary_dict["rRNA"] = rRna
        summary_dict["tRNA"] = tRna
        summary_dict["annotated protein"] = nb_annotation
        summary_dict["transduction function"] = nb_transduction
        summary_dict["replication"] = nb_replication
        summary_dict["recombination"] = nb_recombination
        summary_dict["repair"] = nb_repair
        summary_dict["replication,recombination and repair"] = nb_replication + nb_recombination + nb_repair
        summary_dict["methylation"] = nb_methyl
        summary_dict["GC%"] = round(((seq_c+seq_g)/summary_dict["length"])*100, 2)

        return summary_dict

    def insert_to_df(self, df, column, dico):
        df[column] = dico.values()
        return df

    def apply_to_genBank_list(self):
        for i in self.file_genBank_list:
            result = self.count_nb(i)
            self.result_list.append(result)

        return self.result_list

    def apply_to_eggNog_list(self):
        iterator = 0
        egg_list = []
        for i in self.file_eggCog_list:
            result = self.cog_search(i, iterator)
            egg_list.append(result)
            iterator += 1
        return egg_list

    def create_egg_df(self, egg_list):
        class_list = []
        specie_list = []
        for each_dict in egg_list:
            specie_list.append(each_dict['Specie'])
            for k, v in each_dict.items():
                if k not in class_list:
                    class_list.append(k)

        class_list.pop(0)
        egg_df = pd.DataFrame(index=specie_list, columns=class_list)

        for each_dict in egg_list:
            for k, v in each_dict.items():
                for col in egg_df.columns:
                    for index in egg_df.index:
                        if col == k and index == each_dict['Specie']:
                            egg_df.at[index, col] = v

        return egg_df

    def create_df(self, result_list):
        df_total = pd.DataFrame.from_dict(result_list[0], orient='index')
        for i in range(1, len(result_list)):
            self.insert_to_df(df_total, i, result_list[i])
        df_total = (df_total.transpose())
        df_total.set_index("Specie", inplace=True)
        return df_total

    def export_df(self, df1, df2, df3, filename):
        df1.to_excel(filename, index=True)
        with pd.ExcelWriter(filename) as writer:
            df1.to_excel(writer, sheet_name='main_table')
            df2.to_excel(writer, sheet_name='eggNOG_result')
            df3.to_excel(writer, sheet_name='NCBI_COG_result')

    def cog_search(self, file, iterator):
        """open tsv file with all datas and then implement in the database"""
        df = pd.read_excel(file)
        max_1 = 0
        max_2 = 0
        max_3 = 0
        max_4 = 0
        max_5 = 0
        total_COG = 0
        dataset = {"Specie": self.specie[iterator]}

        for letter in string.ascii_uppercase:
            if letter != "S" and letter != "R":
                letter_count = df["COG_category"].str.count(letter).sum()
                total_COG += letter_count
                if letter_count > max_1:
                    max_1 = letter_count
                    letter_1 = letter
                elif letter_count > max_2:
                    max_2 = letter_count
                    letter_2 = letter
                elif letter_count > max_3:
                    max_3 = letter_count
                    letter_3 = letter
                elif letter_count > max_4:
                    max_4 = letter_count
                    letter_4 = letter
                elif letter_count > max_5:
                    max_5 = letter_count
                    letter_5 = letter

        dataset["Total COG"] = int(total_COG)
        dataset[letter_1] = int(max_1)
        dataset[letter_2] = int(max_2)
        dataset[letter_3] = int(max_3)
        dataset[letter_4] = int(max_4)
        dataset[letter_5] = int(max_5)

        return dataset
