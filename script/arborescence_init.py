import os

# TODO: check if 'result' file is initialise during tree_initialisation

def tree_initialisation():
    root_path = "../"
    species_data = {}
    # TODO Mettre le nom du fichier de l'espèce cible
    specie_target = "Methanosphaera_stadtmanae_MCYC-HGUT-02164"
    potential_target = {1: "Methanosphaera_stadtmanae_MCYC-HGUT-02164"}

    if specie_target == "":
        print("You can chose a known specie")
        answer = input("Choose a key or enter your target specie file name: ")
        for key in potential_target.keys():
            if answer == str(key):
                specie_target = potential_target[int(answer)]
                break
        else:
            specie_target = answer

    # Target specie
    for dir_root in os.listdir(root_path):
        if dir_root == "Target":
            species_data[dir_root] = dict()
            for target in os.listdir(root_path + f"{dir_root}/"):
                if target == specie_target:
                    for dir in os.listdir(root_path + f"{dir_root}/{target}"):
                        if dir == "protein_fasta":
                            for element in dir:
                                species_data["Target"][
                                    str(dir)] = root_path + f"{dir_root}/{target}/{str(dir)}/{element}"
                        if dir == "cds_genomic":
                            for element in os.listdir(root_path + f"{dir_root}/{target}/{dir}"):
                                species_data["Target"][
                                    str(dir)] = root_path + f"{dir_root}/{target}/{str(dir)}/{element}"
                        if dir == "geneBank_summary":
                            for element in os.listdir(root_path + f"{dir_root}/{target}/{dir}"):
                                species_data["Target"][
                                    str(dir)] = root_path + f"{dir_root}/{target}/{str(dir)}/{element}"
                        if dir == "protein_fasta":
                            for element in os.listdir(root_path + f"{dir_root}/{target}/{dir}"):
                                species_data["Target"][
                                    str(dir)] = root_path + f"{dir_root}/{target}/{str(dir)}/{element}"
                        if dir == "genomic_fasta":
                            for element in os.listdir(root_path + f"{dir_root}/{target}/{dir}"):
                                species_data["Target"][
                                    str(dir)] = root_path + f"{dir_root}/{target}/{str(dir)}/{element}"
                        if dir == "eggNog":
                            for element in os.listdir(root_path + f"{dir_root}/{target}/{dir}"):
                                species_data["Target"][
                                    str(dir)] = root_path + f"{dir_root}/{target}/{str(dir)}/{element}"
                        if dir == "cog_database":
                            for element in os.listdir(root_path + f"{dir_root}/{target}/{dir}"):
                                species_data["Target"][
                                    str(dir)] = root_path + f"{dir_root}/{target}/{str(dir)}/{element}"
                        if dir == "Codon_usage":
                            species_data["Target"][
                                str(dir) + "_cusp"] = root_path + f"{dir_root}/{target}/{str(dir)}/Target.cusp"
                            species_data["Target"][
                                str(dir) + "_cai"] = root_path + f"{dir_root}/{target}/{str(dir)}/Target.cai"
                        if dir == "comp_seq":
                            species_data["Target"][
                                str(dir)] = root_path + f"{dir_root}/{target}/{str(dir)}/Target.compseq"
                        if dir == "paf_file":
                            for element in os.listdir(root_path + f"{dir_root}/{target}/{dir}"):
                                species_data["Target"][
                                    str(dir)] = root_path + f"{dir_root}/{target}/{str(dir)}/{element}"

    # Espece proche
    for dir_root in os.listdir(root_path + "especes_proche"):
        species_data[dir_root] = dict()
        for dir in os.listdir(root_path + f"especes_proche/{str(dir_root)}"):
            if dir == "protein_fasta":
                for element in os.listdir(root_path + f"especes_proche/{str(dir_root) + '/' + str(dir)}"):
                    species_data[str(dir_root)][
                        str(dir)] = root_path + f"especes_proche/{str(dir_root) + '/' + str(dir) + '/' + element}"
            if dir == "cds_genomic":
                for element in os.listdir(root_path + f"especes_proche/{str(dir_root) + '/' + str(dir)}"):
                    species_data[str(dir_root)][
                        str(dir)] = root_path + f"especes_proche/{str(dir_root) + '/' + str(dir) + '/' + element}"
            if dir == "geneBank_summary":
                for element in os.listdir(root_path + f"especes_proche/{str(dir_root) + '/' + str(dir)}"):
                    species_data[str(dir_root)][
                        str(dir)] = root_path + f"especes_proche/{str(dir_root) + '/' + str(dir) + '/' + element}"
            if dir == "genomic_fasta":
                for element in os.listdir(root_path + f"especes_proche/{str(dir_root) + '/' + str(dir)}"):
                    species_data[str(dir_root)][
                        str(dir)] = root_path + f"especes_proche/{str(dir_root) + '/' + str(dir) + '/' + element}"
            if dir == "eggNog":
                for element in os.listdir(root_path + f"especes_proche/{str(dir_root) + '/' + str(dir)}"):
                    species_data[str(dir_root)][
                        str(dir)] = root_path + f"especes_proche/{str(dir_root) + '/' + str(dir) + '/' + element}"
            if dir == "Codon_usage":
                species_data[str(dir_root)][
                    str(dir) + "_cusp"] = root_path + f"especes_proche/{dir_root}/{str(dir)}/{dir_root}.cusp"
                species_data[str(dir_root)][
                    str(dir) + "_cai"] = root_path + f"especes_proche/{dir_root}/{str(dir)}/{dir_root}.cai"
            if dir == "comp_seq":
                species_data[str(dir_root)][
                    str(dir)] = root_path + f"especes_proche/{dir_root}/{str(dir)}/{dir_root}.compseq"
            if dir == "paf_file":
                for element in os.listdir(root_path + f"especes_proche/{str(dir_root) + '/' + str(dir)}"):
                    species_data[str(dir_root)][
                        str(dir)] = root_path + f"especes_proche/{str(dir_root) + '/' + str(dir) + '/' + element}"

    # Genre proche
    for dir_root in os.listdir(root_path + "genre_proche"):
        species_data[dir_root] = dict()
        for dir in os.listdir(root_path + f"genre_proche/{str(dir_root)}"):
            if dir == "protein_fasta":
                for element in os.listdir(root_path + f"genre_proche/{str(dir_root) + '/' + str(dir)}"):
                    species_data[str(dir_root)][
                        str(dir)] = root_path + f"genre_proche/{str(dir_root) + '/' + str(dir) + '/' + element}"
            if dir == "cds_genomic":
                for element in os.listdir(root_path + f"genre_proche/{str(dir_root) + '/' + str(dir)}"):
                    species_data[str(dir_root)][
                        str(dir)] = root_path + f"genre_proche/{str(dir_root) + '/' + str(dir) + '/' + element}"
            if dir == "geneBank_summary":
                for element in os.listdir(root_path + f"genre_proche/{str(dir_root) + '/' + str(dir)}"):
                    species_data[str(dir_root)][
                        str(dir)] = root_path + f"genre_proche/{str(dir_root) + '/' + str(dir) + '/' + element}"
            if dir == "eggNog":
                for element in os.listdir(root_path + f"genre_proche/{str(dir_root) + '/' + str(dir)}"):
                    species_data[str(dir_root)][
                        str(dir)] = root_path + f"genre_proche/{str(dir_root) + '/' + str(dir) + '/' + element}"
            if dir == "genomic_fasta":
                for element in os.listdir(root_path + f"genre_proche/{str(dir_root) + '/' + str(dir)}"):
                    species_data[str(dir_root)][
                        str(dir)] = root_path + f"genre_proche/{str(dir_root) + '/' + str(dir) + '/' + element}"
            if dir == "Codon_usage":
                species_data[str(dir_root)][
                    str(dir) + "_cusp"] = root_path + f"genre_proche/{dir_root}/{str(dir)}/{dir_root}.cusp"
                species_data[str(dir_root)][
                    str(dir) + "_cai"] = root_path + f"genre_proche/{dir_root}/{str(dir)}/{dir_root}.cai"
            if dir == "comp_seq":
                species_data[str(dir_root)][
                    str(dir)] = root_path + f"genre_proche/{dir_root}/{str(dir)}/{dir_root}.compseq"
            if dir == "paf_file":
                for element in os.listdir(root_path + f"genre_proche/{str(dir_root) + '/' + str(dir)}"):
                    species_data[str(dir_root)][
                        str(dir)] = root_path + f"genre_proche/{str(dir_root) + '/' + str(dir) + '/' + element}"

    return species_data
