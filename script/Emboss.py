import os
import matplotlib.pyplot as plt
import pandas as pd


class Emboss:
    @staticmethod
    def compseq_run(input_path: str, output_path: str, word_length: int):
        print("compseq")
        os.system(f"compseq -sequence {input_path} -word {word_length} -outfile {output_path}")

    @staticmethod
    def codon_usage_run(fasta_file: str, cusp_output_file: str, cai_output_file: str):
        os.system(f"cusp -sequence {fasta_file} -outfile {cusp_output_file}")
        print("cai")
        os.system(f"cai -seqall {fasta_file} -cfile {cusp_output_file} -outfile {cai_output_file}")

    @staticmethod
    def cai_mean(cai_file: str):
        mean = 0
        number_sequence = 0
        with open(cai_file, "r") as handle:
            for line in handle.readlines():
                number_sequence += 1
                mean += float(line.split(" ")[3].replace("\n", ""))
        mean /= number_sequence
        return mean

    @staticmethod
    def cai_range(cai_file: str):
        data_range = {"0-0.2": 0, "0.2-0.4": 0, "0.4-0.6": 0, "0.6-0.8": 0, "0.8-1": 0}

        with open(cai_file, "r") as handle:
            for line in handle.readlines():
                value = float(line.split(" ")[3].replace("\n", ""))
                if 0 <= value <= 0.2:
                    data_range["0-0.2"] += 1
                if 0.2 < value <= 0.4:
                    data_range["0.2-0.4"] += 1
                if 0.4 < value <= 0.6:
                    data_range["0.4-0.6"] += 1
                if 0.6 < value <= 0.8:
                    data_range["0.6-0.8"] += 1
                if 0.8 < value <= 1:
                    data_range["0.8-1"] += 1
        return data_range

    @staticmethod
    def cai_search_corresponding_protein(cai_path: str, cds_genomic_file: str, specie: str):
        corresponding = {}
        genomic_protein = {}
        with open(cds_genomic_file, "r") as genomic_handle:
            for line in genomic_handle.readlines():
                if line.startswith(">"):
                    row = line.split("[")
                    sequence = row[0].replace(">lcl|", "").replace(" ", "")
                    for element in row:
                        if element.startswith("protein="):
                            protein = element.replace("]", "").replace("protein=", "")
                    genomic_protein[sequence] = protein

        with open(cai_path, "r") as cai_handle:
            data = {}
            for line in cai_handle.readlines():
                table = line.split(" ")
                id_sequence = table[1]
                cai = float(table[3].replace("\n", ""))
                data[table[1]] = {"protein": genomic_protein[id_sequence], "CAI": cai}

        # corresponding[specie] = data
        corresponding_dataframe = pd.DataFrame.from_dict(data)
        corresponding_dataframe = corresponding_dataframe.T
        corresponding_dataframe.to_excel(f"../result/Cai_{specie}_detail.xlsx")

    @staticmethod
    def cai_range_plot(pd_dataframe, path=None, show=True, save=False):
        if save is False and path is None:
            raise "Need a path for save the plot"

        figure = pd_dataframe.plot.bar(rot=0, title="Number of Cai score by interval for species")
        figure.set_ylabel("Number of cai score")
        figure.set_xlabel("Cai score interval")

        if save is True:
            figure.get_figure().savefig(path)
        if show is True:
            plt.show()
