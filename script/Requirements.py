import os
import subprocess


def requirements_initialisation(requirement_path):
    with open(str(requirement_path), "r") as requirements:
        requirements_list = requirements.readlines()
        for requirement in requirements_list:
            requirement = str(requirement.replace("\n", ""))
            if subprocess.call(["which", requirement]) == 0:
                os.system(f"pip install -r {requirement}")
