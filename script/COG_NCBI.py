# -*- coding: utf-8 -*-
# @Time : 2022/10/12 22:37
# @Author : Juejun CHEN
import collections
import pandas as pd


class NCBI_COG:

    @staticmethod
    def count_COG_class(COG_file):
        print("Cog class")
        result = pd.read_csv(COG_file, index_col=0, sep='\t')
        class_dico = collections.defaultdict(int)
        class_list = result['Cat'].to_list()

        for i in class_list:
            if len(i) == 1:
                class_dico[i] += 1
            elif len(i) > 1:
                str = i.replace(' ', '')
                for letter in str:
                    class_dico[letter] += 1

        class_df = pd.DataFrame.from_dict(class_dico, orient='index')
        class_df.columns = ['Cat']
        class_df = class_df.sort_values('Cat', ascending=False)

        return class_df

    @staticmethod
    def export_df(df, filename):
        df.to_excel(filename, index=True)
        with pd.ExcelWriter(filename) as writer:
            df.to_excel(writer)
